# frozen_string_literal: true

class Manager2cli
  # Class that handles all communications
  class Comm
    # Class to handles API communications
    class API
      require 'json'
      require 'net/http'

      def initialize
        @host     = 'https://api.insomnia247.nl'
        @key      = ''
        @admin    = false
        @archived = false
      end

      # Try loading the API key from disk
      def loadkey
        @key = File.read("#{Dir.home}/.config/insomnia247/api_key").strip
      end

      # Check to see if we can reach the API at all
      def checkalive
        retries ||= 0
        uri = URI "#{@host}/v1/test"
        res = Net::HTTP.get(uri)
        dat = JSON.parse(res)
        raise 'ApiError' unless dat['status'].zero?
      rescue NoMethodError
        sleep(0.5)
        retry if (retries += 1) < 3
      end

      # Check if API key is valid
      def checkkey
        post '/v1/test/key'
      end

      # Check which permissions we have
      def checkadmin
        data = post '/v1/user/admin', {}, true
        return if data.nil?

        @admin = true if data['status'].zero?
      end

      def isadmin
        @admin
      end

      # Check if the user is archived
      def checkarchived
        data = post '/v1/user/archived', {}, true
        return if data.nil?

        @archived = data['result']
      end

      def isarchived
        @archived
      end

      # Generic API call function
      # rubocop:disable Style/OptionalBooleanParameter
      def post(path, options = {}, failok = false)
        retries ||= 0
        # insert key into options
        options['key'] = @key
        options = prune options
        uri     = URI "#{@host}#{path}"
        res     = Net::HTTP.post_form(uri, options)
        data    = JSON.parse(res.body)

        return data if data['status'].zero?

        puts data['message']
        raise 'ApiError' unless failok
      rescue NoMethodError
        sleep(0.5)
        retry if (retries += 1) < 3
      end

      def submitrequest(path, options)
        post path, options
      end
      # rubocop:enable Style/OptionalBooleanParameter

      private

      # Clear out any empty variables
      def prune(options)
        options.each do |key, value|
          options.delete key if value == ''
        end
        options
      end
    end
  end
end
