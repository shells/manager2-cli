#!/usr/bin/ruby
# frozen_string_literal: true

require_relative 'Comm/api'
require_relative 'UI/mainmenu'

# Main class for the manager CLI
class Manager2cli
  Manager2cli::UI::Mainmenu.new Manager2cli::Comm::API.new
end

Manager2cli.new
