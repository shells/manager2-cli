# frozen_string_literal: true

class Manager2cli
  class UI
    # Class to deal i2am stuff
    class I2am
      def initialize(api)
        @api = api
      end

      def listtunnels
        CLI::UI::Frame.open('{{bold:Warning}}', color: :red) do
          # rubocop:disable Layout/LineLength
          puts CLI::UI.fmt "These tunnels are meant to provide access to people living under oppressive regimes where access to our service is blocked by their government. Please {{bold:DO NOT}} use them for any other purpose.\nFor more information see https://i2.am/"
          # rubocop:enable Layout/LineLength
        end

        CLI::UI::Frame.open('SSH Tunnel list') do
          tunnels = gettunnels

          if tunnels.nil?
            puts 'No tunnels configured'
            return
          end

          tunnels.each do |tunnel|
            puts "IP: #{tunnel[0]},\tPort: #{tunnel[1]}"
          end
        end
      end

      def listtorservices
        CLI::UI::Frame.open('TOR hidden services list') do
          torservices = gettorservices

          if torservices.nil?
            puts 'No TOR hidden services configured'
            return
          end

          torservices.each do |name, config|
            puts "#{name}:\tHost: #{config['host']},\tPort: #{config['port']}"
          end
        end
      end

      private

      def gettunnels
        resultset = Manager2cli::UI.apirequest(
          @api,
          '/v1/i2am/tunnel/list',
          {},
          2
        )['result']

        return nil if resultset == []

        tunnels = []
        resultset.each_value do |v|
          tunnel = [v['remote_ssh_host']]

          if v.key? 'port'
            tunnel.push v['port']
          else
            tunnel.push '443'
          end

          tunnels.push tunnel
        end

        tunnels
      end

      def gettorservices
        resultset = Manager2cli::UI.apirequest(
          @api,
          '/v1/i2am/torservice/list',
          {},
          2
        )['result']

        return nil if resultset == []

        resultset
      end
    end
  end
end
