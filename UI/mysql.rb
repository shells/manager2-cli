# frozen_string_literal: true

class Manager2cli
  class UI
    # Class to deal with MySQL-specific interactions
    class Mysql
      def initialize(api)
        @api = api
      end

      # List configured databases
      def listdatabases
        CLI::UI::Frame.open('MySQL database list') do
          databases = getdatabases

          backout = false
          if databases.nil?
            puts 'No MySQL databases configured'
            backout = true
          end

          databases.each { |database| puts "#{ENV.fetch('USER', nil)}_#{database}" } unless backout
        end
      end

      # Request a new database
      def requestdatabase
        CLI::UI::Frame.open('Request new MySQL database') do
          dbname  = CLI::UI.ask('What will be the database name?', allow_empty: false)
          comment = Manager2cli::UI.getcomment

          Manager2cli::UI.apirequest(
            @api,
            '/v1/mysql/request',
            'dbname'  => dbname,
            'comment' => comment
          )

          puts 'You will receive status updates about your request by e-mail.'
        end
      end

      # Reset the password for a database
      def resetpass
        # rubocop:disable Metrics/BlockLength
        CLI::UI::Frame.open('Reset MySQL password') do
          databases = getdatabases

          backout = false
          if databases.nil?
            puts 'No MySQL databases configured'
            backout = true
          end

          unless backout
            selected = CLI::UI::Prompt.ask('Reset password for which database?') do |handler|
              databases.each { |database| handler.option(database) { "#{ENV.fetch('USER', nil)}_#{database}" } }
              handler.option('Go back') { backout = true }
            end
          end

          # Make API call
          unless backout
            result = Manager2cli::UI.apirequest(
              @api,
              '/v1/mysql/password/reset',
              { 'dbname' => selected },
              1
            )

            # Show result of API call
            # rubocop:disable Layout/LineEndStringConcatenationIndentation
            # rubocop:disable Layout/LineContinuationSpacing
            if result['status'].zero?
              Manager2cli::UI.printsuccess "#{result['message']}\n"\
                "Database name: #{result['result']['db name']}\n"\
                "Database user: #{result['result']['db user']}\n"\
                "New password: #{result['result']['db pass']}"
              Manager2cli::UI.puppetmessage
            else
              Manager2cli::UI.printerror result['message']
            end
            # rubocop:enable Layout/LineContinuationSpacing
            # rubocop:enable Layout/LineEndStringConcatenationIndentation
          end
        end
        # rubocop:enable Metrics/BlockLength
      end

      private

      # Helper function to get listing of configured databases
      def getdatabases
        resultset = Manager2cli::UI.apirequest(
          @api,
          '/v1/mysql/list',
          {},
          2
        )['result']

        return nil if resultset == []

        resultset
      end
    end
  end
end
