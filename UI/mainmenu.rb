# frozen_string_literal: true

require_relative 'accounts'
require_relative 'apache'
require_relative 'i2am'
require_relative 'limits'
require_relative 'mysql'
require_relative 'packages'
require_relative 'requests'

class Manager2cli
  # Class contains main menu structure
  class UI
    require 'cli/ui'
    CLI::UI::StdoutRouter.enable

    # Some helper methods

    # Generic printers
    def self.printerror(message)
      CLI::UI::Frame.open('{{bold:Error}}', color: :red) do
        puts message
      end
    end

    def self.printsuccess(message)
      CLI::UI::Frame.open('{{bold:Success}}', color: :green) do
        puts message
      end
    end

    def self.puppetmessage
      CLI::UI::Frame.open('Notice', color: :yellow) do
        puts 'Your requested change may take up to 30 minutes to be provisioned.'
      end
    end

    # Create spinner to wait while API request is being processed
    def self.apirequest(api, path, options, type = 0)
      result  = nil
      pending = [
        'Submitting request',
        'Submitting change',
        'Retrieving data'
      ][type]
      CLI::UI::SpinGroup.new do |sg|
        sg.add(pending) do |spinner|
          result = api.submitrequest path, options
          spinner.update_title(result['message'])
        end
      end

      result
    end

    # Generic function to add a comment to any request
    def self.getcomment
      CLI::UI.ask('Any extra comments or remarks you wish to add? (Leave blank for none)', allow_empty: true)
    end

    # Class contains main menu structure
    class Mainmenu
      def initialize(api)
        @api  = api
        @quit = false
        @api.loadkey

        CLI::UI::Frame.open('{{bold:Manager}}', color: :green) do
          CLI::UI::SpinGroup.new do |sg|
            # sg.add('Loading your API key') { @api.loadkey }
            # sg.wait
            sg.add('Checking API connection') { @api.checkalive }
            sg.add('Checking your API key')   { @api.checkkey }
            # sg.wait
            sg.add('Checking permissions') do |spinner|
              @api.checkadmin
              @api.checkarchived
              spinner.update_title('User permissions granted')
              spinner.update_title('User permissions granted, but account is archived!') if @api.isarchived
              spinner.update_title('Admin permissions granted') if @api.isadmin
            end
          end

          mainmenu until @quit
        rescue Interrupt
          abort 'User aborted the application'
        end
      end

      def mainmenu(function = nil)
        CLI::UI::Frame.open('Main menu') do
          function = CLI::UI::Prompt.ask('What do you want to manage?') do |handler|
            handler.option('Requests')                  { 'requestmenu' } if @api.isadmin
            handler.option('User account')              { 'accountmenu' }
            handler.option('Apache')                    { 'apachemenu' } unless @api.isarchived
            handler.option('MySQL')                     { 'mysqlmenu' } unless @api.isarchived
            handler.option('Software packages')         { 'packagemenu' }
            handler.option('Quotas and limits')         { 'limitmenu' } unless @api.isarchived
            handler.option('Insomnia Midnight Project') { 'i2ammenu' }
            handler.option('Quit')                      { @quit = true }
          end
        end

        # Use send function here so we can close the menu frame before progressing
        send function unless @quit
      end

      def requestmenu(back: false)
        CLI::UI::Frame.open('Requests') do
          requests = Manager2cli::UI::Requests.new @api
          until back
            CLI::UI::Prompt.ask('What do you want to do?') do |handler|
              handler.option('List pending requests') { requests.list }
              handler.option('Approve request')       { requests.approve }
              handler.option('Reject request')        { requests.reject }
              handler.option('Delete request')        { requests.delete }
              handler.option('Go back to main menu')  { back = true }
            end
          end
        end
        mainmenu
      end

      # rubocop:disable Metrics/CyclomaticComplexity
      def accountmenu(back: false)
        CLI::UI::Frame.open('User account') do
          accounts = Manager2cli::UI::Accounts.new @api
          until back
            CLI::UI::Prompt.ask('What do you want to do?') do |handler|
              handler.option('Add new user')                  { accounts.newuser } if @api.isadmin
              handler.option('Look up user email address')    { accounts.lookupemail } if @api.isadmin
              handler.option('Reset user password')           { accounts.resetpassword } if @api.isadmin
              handler.option('Mark user as removed')          { accounts.markremoved } if @api.isadmin
              handler.option('Delete user')                   { accounts.delete } if @api.isadmin
              handler.option('Un-archive my account')         { accounts.unarchive } if @api.isarchived
              handler.option('Show or change login shell')    { accounts.loginshell }
              handler.option('Show or change contact e-mail') { accounts.contactmail }
              handler.option('Configure ssh key only login')  { accounts.sshkeyonly }
              handler.option('Configure 2-factor TOTP login') { accounts.totp }
              handler.option('List pending requests')         { accounts.listrequests }
              handler.option('Invite a new user')             { accounts.invite }
              handler.option('Go back to main menu')          { back = true }
            end
          end
        end
        mainmenu
      end
      # rubocop:enable Metrics/CyclomaticComplexity

      def apachemenu(back: false)
        CLI::UI::Frame.open('Apache') do
          apache = Manager2cli::UI::Apache.new @api
          until back
            CLI::UI::Prompt.ask('What do you want to do?') do |handler|
              handler.option('List VHosts')          { apache.listvhosts }
              handler.option('Enable a VHost')       { apache.enablevhost }
              handler.option('Disable a VHost')      { apache.disablevhost }
              handler.option('Remove a VHost')       { apache.removevhost }
              handler.option('Request a new VHost')  { apache.requestvhost }
              handler.option('Go back to main menu') { back = true }
            end
          end
        end
        mainmenu
      end

      def mysqlmenu(back: false)
        CLI::UI::Frame.open('MySQL') do
          mysql = Manager2cli::UI::Mysql.new @api
          until back
            CLI::UI::Prompt.ask('What do you want to do?') do |handler|
              handler.option('List databases')       { mysql.listdatabases }
              handler.option('Request a database')   { mysql.requestdatabase }
              handler.option('Reset a password')     { mysql.resetpass }
              handler.option('Go back to main menu') { back = true }
            end
          end
        end
        mainmenu
      end

      def packagemenu(back: false)
        CLI::UI::Frame.open('Software packages') do
          packages = Manager2cli::UI::Packages.new @api
          until back
            CLI::UI::Prompt.ask('What do you want to do?') do |handler|
              handler.option('Request package installation') { packages.requestinstall }
              handler.option('Go back to main menu')         { back = true }
            end
          end
        end
        mainmenu
      end

      def limitmenu(back: false)
        CLI::UI::Frame.open('Quotas and limits') do
          limits = Manager2cli::UI::Limits.new @api
          until back
            CLI::UI::Prompt.ask('What do you want to do?') do |handler|
              handler.option('Show limits')                    { limits.list }
              handler.option('Request raised disk quota')      { limits.requestquota }
              handler.option('Request raised process limit')   { limits.requestprocess }
              handler.option('Request raised memory limit')    { limits.requestmemory }
              handler.option('Go back to main menu')           { back = true }
            end
          end
        end
        mainmenu
      end

      def i2ammenu(back: false)
        CLI::UI::Frame.open('Insomnia Midnight Project') do
          i2am = Manager2cli::UI::I2am.new @api
          until back
            CLI::UI::Prompt.ask('What do you want to do?') do |handler|
              handler.option('Show available SSH tunnels') { i2am.listtunnels }
              handler.option('Show TOR hidden services')   { i2am.listtorservices }
              handler.option('Go back to main menu')       { back = true }
            end
          end
        end
        mainmenu
      end
    end
  end
end
