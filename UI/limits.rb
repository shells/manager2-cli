# frozen_string_literal: true

class Manager2cli
  class UI
    # Class to deal with quotas and limits
    class Limits
      def initialize(api)
        @api = api
      end

      # Show a list of configured limits
      def list
        CLI::UI::Frame.open('Current limits') do
          limits = Manager2cli::UI.apirequest(
            @api,
            '/v1/limit/list',
            {},
            2
          )['result']

          limits.each do |key, value|
            if key == 'disk'
              puts 'disk:'
              puts "  SSD array  : #{getquotas}"
              puts '  Coldstorage: No quotas'
            else
              puts "#{key}: #{value}"
            end
          end
        end
      end

      # Request raising the disk quota
      def requestquota
        CLI::UI::Frame.open('Request new disk quota') do
          limit   = CLI::UI.ask('What should the new quota be? (in megabytes)', allow_empty: false)
          comment = Manager2cli::UI.getcomment

          Manager2cli::UI.apirequest(
            @api,
            '/v1/limit/disk/request',
            'limit'   => limit.to_s,
            'comment' => comment
          )

          puts 'You will receive status updates about your request by e-mail.'
        end
      end

      # Request relaxing the process limit
      def requestprocess
        CLI::UI::Frame.open('Request raised process limit') do
          CLI::UI::Prompt.ask('Request raised limit?') do |handler|
            handler.option('Yes') { nil }
            handler.option('No') { return nil }
          end
          comment = Manager2cli::UI.getcomment

          Manager2cli::UI.apirequest(
            @api,
            '/v1/limit/process/request',
            'comment' => comment
          )

          puts 'You will receive status updates about your request by e-mail.'
        end
      end

      # Request higher memory cap
      def requestmemory
        CLI::UI::Frame.open('Request new memory limit') do
          limit   = CLI::UI.ask('What should the new limit be? (in gigabytes)', allow_empty: false)
          comment = Manager2cli::UI.getcomment

          Manager2cli::UI.apirequest(
            @api,
            '/v1/limit/memory/request',
            'limit'   => limit.to_s,
            'comment' => comment
          )

          puts 'You will receive status updates about your request by e-mail.'
        end
      end

      private

      def getquotas
        return 'No quotas' unless system 'which quota > /dev/null 2>&1'

        res = `quota -s -g #{ENV.fetch('USER', nil)}`.split("\n").grep(/md0/).first
        res = res.split unless res.nil?

        return 'No quotas' if res.nil?

        "used: #{res[1]}, quota: #{res[2]}"
      end
    end
  end
end
