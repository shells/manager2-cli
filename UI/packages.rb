# frozen_string_literal: true

class Manager2cli
  class UI
    # Class to deal with software packages
    class Packages
      require 'debian/apt_pkg'

      def initialize(api)
        @api = api
      end

      # rubocop:disable Metrics/BlockLength
      def requestinstall
        CLI::UI::Frame.open('Request package installation') do
          puts CLI::UI.fmt '{{yellow:Packages from the main debian stable repo {{bold:only}}.}}'

          package = CLI::UI.ask('What package should be installed?', allow_empty: false)

          Debian::AptPkg.init
          Debian::AptPkg::PkgCache.update
          pkg_list = Debian::AptPkg::PkgCache.packages.select { |pkg| pkg.name.match package }.map(&:name)

          if pkg_list.empty?
            puts CLI::UI.fmt '{{red:No such package found in the Debian repo!}}'
            return
          end

          unless pkg_list.include? package
            package = CLI::UI::Prompt.ask('No exact match found. Perhaps you meant one of these?') do |handler|
              pkg_list.each { |pkg| handler.option(pkg) { pkg } }
            end
          end

          # puts CLI::UI.fmt "\n\n{{yellow:If you are not sure, pick {{bold:No}} here.}}"
          # backports = CLI::UI::Prompt.ask('Should the package be installed from backports?') do |handler|
          #   handler.option('No')  { false }
          #   handler.option('Yes') { true }
          # end
          backports = false
          comment = Manager2cli::UI.getcomment

          params = {
            'package' => package,
            'comment' => comment
          }
          params['backports'] = true if backports

          Manager2cli::UI.apirequest(
            @api,
            '/v1/package/request',
            params
          )

          puts 'You will receive status updates about your request by e-mail.'
        end
        # rubocop:enable Metrics/BlockLength
      end
    end
  end
end
