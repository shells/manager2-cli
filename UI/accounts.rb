# frozen_string_literal: true

class Manager2cli
  class UI
    # Class to deal with user accounts
    class Accounts
      def initialize(api)
        @api = api
      end

      def newuser
        CLI::UI::Frame.open('Add new user') do
          username = CLI::UI.ask('Username for new user?', allow_empty: false)

          Manager2cli::UI.apirequest(
            @api,
            '/v1/user/new',
            { 'username' => username },
            1
          )
        end
      end

      # Look up a users email address
      def lookupemail
        CLI::UI::Frame.open('Lookup user e-mail address') do
          username  = CLI::UI.ask('Lookup for which user?', allow_empty: false)
          resultset = Manager2cli::UI.apirequest(
            @api,
            '/v1/user/email/lookup',
            { 'username' => username },
            2
          )['result']

          CLI::UI::Frame.open('Contact e-mail address') { puts resultset }
        end
      end

      # Reset user password
      def resetpassword
        CLI::UI::Frame.open('Reset password') do
          username = CLI::UI.ask('Reset for which user?', allow_empty: false)

          Manager2cli::UI.apirequest(
            @api,
            '/v1/user/password/reset',
            { 'username' => username },
            1
          )
        end
      end

      # Mar user as removed
      def markremoved
        CLI::UI::Frame.open('Mark user as removed') do
          username = CLI::UI.ask('Which username?', allow_empty: false)

          Manager2cli::UI.apirequest(
            @api,
            '/v1/user/remove',
            { 'username' => username },
            1
          )
        end
      end

      # Really delete user account
      def delete
        CLI::UI::Frame.open('Delete user') do
          username = CLI::UI.ask('Username to delete?', allow_empty: false)

          Manager2cli::UI.apirequest(
            @api,
            '/v1/user/delete',
            { 'username' => username },
            1
          )
        end
      end

      # View/change login shell
      def loginshell
        CLI::UI::Frame.open('Login shell') do
          resultset = Manager2cli::UI.apirequest(
            @api,
            '/v1/user/shell/get',
            {},
            2
          )['result']

          # Show current shell
          CLI::UI::Frame.open('Current login shell') { puts resultset }

          # Provide early out for no change
          backout  = false
          selected = CLI::UI::Prompt.ask('Change your login shell?') do |handler|
            shells = File.readlines('/etc/shells').delete_if { |line| line.start_with? '#' }
            shells = shells.delete_if { |line| line.start_with?('/usr/') && shells.include?(line.delete_prefix('/usr')) }.sort
            shells.each do |shell|
              shell.chomp!
              handler.option(shell) { shell }
            end
            handler.option('Go back without changing login shell') { backout = true }
          end

          unless backout
            Manager2cli::UI.apirequest(
              @api,
              '/v1/user/shell/set',
              { 'shell' => selected },
              1
            )
          end
        end
      end

      # View/change contact email address
      def contactmail
        CLI::UI::Frame.open('Contact e-mail') do
          resultset = Manager2cli::UI.apirequest(
            @api,
            '/v1/user/email/get',
            {},
            2
          )['result']

          # Show current address
          CLI::UI::Frame.open('Current contact e-mail') do
            puts resultset
          end

          # Provide early out for no change
          backout = false
          CLI::UI::Prompt.ask('Change your contact e-mail?') do |handler|
            handler.option('Yes') { '' }
            handler.option('No') { backout = true }
          end

          # Change address
          unless backout
            email = CLI::UI.ask('What should be the new e-mail address?', allow_empty: false)
            Manager2cli::UI.apirequest(
              @api,
              '/v1/user/email/set',
              { 'email' => email },
              1
            )
          end
        end
      end

      # List pending requests
      def listrequests
        CLI::UI::Frame.open('Pending requests') do
          resultset = Manager2cli::UI.apirequest(
            @api,
            '/v1/user/requests/list',
            {},
            2
          )['result']

          puts 'No pending requests' if resultset['requests'] == []

          resultset['requests'].each do |request|
            CLI::UI::Frame.divider('')
            hashprint request
          end
        end
      end

      # Send out an invite
      def invite
        CLI::UI::Frame.open('Send out an invite') do
          email = CLI::UI.ask('What e-mail address should the invite be sent to?', allow_empty: false)

          Manager2cli::UI.apirequest(
            @api,
            '/v1/user/invite/new',
            { 'email' => email },
            1
          )
        end
      end

      # Un-archive account
      def unarchive
        CLI::UI::Frame.open('Un-archiving your account ...') do
          Manager2cli::UI.apirequest(
            @api,
            '/v1/user/unarchive',
            {}
          )['result']
        end
      end

      # Configure "ssh key only" login
      def sshkeyonly
        CLI::UI::Frame.open('Ssh key only login configuration') do
          state = nil
          CLI::UI::Frame.open('Current status') do
            state = Manager2cli::UI.apirequest(@api, '/v1/user/auth/sshonly/check', {}, 2)['result']
            true
          end

          unless state
            CLI::UI::Frame.open('WARNING', color: :yellow) do
              puts CLI::UI.fmt 'If you do not have an SSH key configured already, you will {{red:lock yourself out of your account!}}'
              puts ''
              puts 'This setting only applies to SSH.'
              puts 'Other services (ftp, nextcloud, mail etc.) are unaffected by this setting.'
            end
          end

          # Provide early out for no change
          backout = false
          CLI::UI::Prompt.ask('Toggle SSH key only logins?') do |handler|
            handler.option('Yes') { '' }
            handler.option('No')  { backout = true }
          end

          # Update setting
          unless backout
            call = state ? 'disable' : 'enable'

            Manager2cli::UI.apirequest(@api, "/v1/user/auth/sshonly/#{call}", {}, 1)
          end
        end
      end

      def totp
        CLI::UI::Frame.open('TOTP configuration') do
          puts 'We will now run the external "google-authenticator" command to let you'
          puts 'configure your TOTP token app. (Google Authenticator, Authy, FreeOTP etc.)'
          puts ''
          puts 'This setting only applies to SSH.'
          puts 'Other services (ftp, nextcloud, mail etc.) are unaffected by this setting.'
        end

        pid = spawn('/usr/bin/google-authenticator')
        Process.wait pid

        CLI::UI::Frame.open('TOTP configuration finished') do
          puts 'If all went well you should now get a "Verification code" prompt before'
          puts 'your SSH password prompt.'
          puts ''
          puts 'Be sure to save your scratch codes in a secure place. They will let you'
          puts 'access your account if you lose/wipe your phone etc.'
          puts ''
          puts 'To disable 2-factor TOTP, move/delete your ~/.google_authenticator file.'
        end
      end

      private

      def hashprint(item, level = 0)
        item.each do |key, value|
          if value.instance_of?(Hash)
            print '  ' * level
            puts "#{key}:"
            hashprint value, level + 1
          else
            next if key == 'user'

            print '  ' * level
            puts "#{key}: #{value}"
          end
        end
      end
    end
  end
end
