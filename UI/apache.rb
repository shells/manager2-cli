# frozen_string_literal: true

class Manager2cli
  class UI
    # Class to deal with apache-specific interactions
    class Apache
      require 'resolv'

      def initialize(api)
        @api = api
      end

      # Show listing of configured vhosts
      def listvhosts
        CLI::UI::Frame.open('VHost list') do
          vhosts = getvhostswithstate

          if vhosts.nil?
            puts 'No VHosts configured'
            return
          end

          vhosts.each { |vhost, state| puts CLI::UI.fmt "#{vhost} (#{statecolourizer(state)})" }
        end
      end

      # Enable a vhost
      def enablevhost
        vhosts = getvhostsnotenabled

        if vhosts == []
          puts CLI::UI.fmt '{{yellow:No disabled VHosts configured}}'
          return
        end

        selected = CLI::UI::Prompt.ask('Enable which VHost?') do |handler|
          vhosts.each { |vhost| handler.option(vhost) { vhost } }
          handler.option('Go back') { return nil }
        end

        return unless vhostipchecks(selected)

        # Make API call
        result = Manager2cli::UI.apirequest(
          @api,
          '/v1/apache/vhost/enable',
          { 'domain' => selected },
          1
        )

        # Show result of API call
        if result['status'].zero?
          Manager2cli::UI.printsuccess result['message']
          Manager2cli::UI.puppetmessage
        else
          Manager2cli::UI.printerror result['message']
        end
      end

      # Disable a vhost
      def disablevhost
        vhosts = getvhostsenabled

        if vhosts == []
          puts CLI::UI.fmt '{{yellow:No enabled VHosts configured}}'
          return
        end

        puts "\nConfig will remain available for re-enabling of VHost"
        puts 'Will leave data in the documentroot of the VHost untouched.'
        selected = CLI::UI::Prompt.ask('Disable which VHost?') do |handler|
          vhosts.each { |vhost| handler.option(vhost) { vhost } }
          handler.option('Go back') { return nil }
        end

        # Make API call
        result = Manager2cli::UI.apirequest(
          @api,
          '/v1/apache/vhost/disable',
          { 'domain' => selected },
          1
        )

        # Show result of API call
        if result['status'].zero?
          Manager2cli::UI.printsuccess result['message']
          Manager2cli::UI.puppetmessage
        else
          Manager2cli::UI.printerror result['message']
        end
      end

      # Remove a vhost
      def removevhost
        vhosts = getvhostsenabled + getvhostsdisabled

        if vhosts == []
          puts CLI::UI.fmt '{{yellow:No enabled VHosts configured}}'
          return
        end

        puts "\nConfig will remain available for re-enabling of VHost"
        puts CLI::UI.fmt '{{red:Will {{bold:DELETE ALL DATA}} in the documentroot of the VHost!}}'
        selected = CLI::UI::Prompt.ask('Remove which VHost?') do |handler|
          vhosts.each { |vhost| handler.option(vhost) { vhost } }
          handler.option('Go back') { return nil }
        end

        # Make API call
        result = Manager2cli::UI.apirequest(
          @api,
          '/v1/apache/vhost/remove',
          { 'domain' => selected },
          1
        )

        # Show result of API call
        if result['status'].zero?
          Manager2cli::UI.printsuccess result['message']
          Manager2cli::UI.puppetmessage
        else
          Manager2cli::UI.printerror result['message']
        end
      end

      # Request configuration of a new vhost
      def requestvhost
        CLI::UI::Frame.open('Request new VHost') do
          puts 'If you are requesting a new subdomain (mynewdomain.insomnia247.nl), please'
          puts 'include the ".insomnia247.nl" part in your request.'
          puts ''
          puts 'If you are requesting a custom domain (mynewdomain.com), please make sure your'
          puts 'DNS records are configured before submitting your request.'
          puts ''
          domain       = CLI::UI.ask('What will be the domain name?', allow_empty: false)
          documentroot = CLI::UI.ask('Custom path for your documentroot? (Leave blank for default)', allow_empty: true)
          strongtls    = CLI::UI::Prompt.ask('TLS configuration?') do |handler|
            handler.option('Normal') { false }
            handler.option('Strong') { true }
          end
          comment = Manager2cli::UI.getcomment

          return unless vhostipchecks(domain)

          Manager2cli::UI.apirequest(
            @api,
            '/v1/apache/vhost/request',
            'domain'       => domain,
            'documentroot' => documentroot,
            'strongtls'    => strongtls,
            'comment'      => comment
          )

          puts 'You will receive status updates about your request by e-mail.'
        end
      end

      private

      # Helper methods to get a list of vhosts
      def getvhosts
        getvhostswithstate.keys
      end

      def getvhostsenabled
        getvhostswithstate.map { |k, v| k if v == 'enabled' }.compact
      end

      def getvhostsdisabled
        getvhostswithstate.map { |k, v| k if v == 'disabled' }.compact
      end

      def getvhostsnotenabled
        getvhostswithstate.map { |k, v| k unless v == 'enabled' }.compact
      end

      def statecolourizer(state)
        colours = {
          'enabled'  => 'green',
          'disabled' => 'yellow',
          'removed'  => 'red'
        }

        "{{#{colours[state]}:#{state}}}"
      end

      def statehelper(vhostdata)
        state = 'enabled'
        state = 'disabled' if vhostdata.key? 'disabled'
        state = 'removed' if vhostdata.key? 'removed'
        state
      end

      def getvhostswithstate
        resultset = Manager2cli::UI.apirequest(
          @api,
          '/v1/apache/vhost/list',
          {},
          2
        )['result']

        return nil if resultset == []

        fulllist = {}
        resultset[0]['apache_vhosts'].each { |vhost, data| fulllist[vhost] = statehelper(data) } if resultset.first.key? 'apache_vhosts'
        resultset[1]['apache_proxies'].each { |vhost, data| fulllist[vhost] = statehelper(data) } if resultset.last.key? 'apache_proxies'

        fulllist
      end

      # Function to see if DNS config is in order
      # rubocop:disable Metrics/PerceivedComplexity
      # rubocop:disable Metrics/CyclomaticComplexity
      def vhostipchecks(vhost, correct: true)
        return true if vhost.end_with? '.insomnia247.nl'

        addrs    = Resolv.getaddresses vhost
        addrswww = Resolv.getaddresses "www.#{vhost}"

        expected4 = IPAddr.new '141.95.28.213'
        expected6 = IPAddr.new '2001:41d0:701:1100::2d17'

        ipaddrs    = addrs.map { |e| IPAddr.new e }
        ipaddrswww = addrswww.map { |e| IPAddr.new e }

        # Check if IPv4 records are OK
        if ipaddrs.include? expected4
          puts CLI::UI.fmt "{{green:Correct IPv4 address found for '#{vhost}'}}"
        else
          puts CLI::UI.fmt "{{red:Incorrect IPv4 address found for '#{vhost}'}}     (Expected: #{expected4})"
          correct = false
        end

        if ipaddrswww.include? expected4
          puts CLI::UI.fmt "{{green:Correct IPv4 address found for 'www.#{vhost}'}}"
        else
          puts CLI::UI.fmt "{{red:Incorrect IPv4 address found for 'www.#{vhost}'}} (Expected: #{expected4})"
          correct = false
        end

        # Check if IPv6 records are OK
        if ipaddrs.include? expected6
          puts CLI::UI.fmt "{{green:Correct IPv6 address found for '#{vhost}'}}"
        else
          puts CLI::UI.fmt "{{red:Incorrect IPv6 address found for '#{vhost}'}}     (Expected: #{expected6})"
          correct = false
        end

        if ipaddrswww.include? expected6
          puts CLI::UI.fmt "{{green:Correct IPv6 address found for 'www.#{vhost}'}}"
        else
          puts CLI::UI.fmt "{{red:Incorrect IPv6 address found for 'www.#{vhost}'}} (Expected: #{expected6})"
          correct = false
        end

        unless correct
          puts ''
          puts "DNS records found for #{vhost}:     #{ipaddrs.join(', ')}"
          puts "DNS records found for www.#{vhost}: #{ipaddrswww.join(', ')}"
          puts ''

          action = CLI::UI::Prompt.ask('What would you like to do?') do |handler|
            handler.option('Retry') { 'r' }
            handler.option('Ignore') { 'i' }
            handler.option('Abort') { 'a' }
          end

          case action
          when 'i'
            return true
          when 'a'
            return false
          when 'r'
            return vhostipchecks(vhost)
          end
        end
        true
      end
      # rubocop:enable Metrics/CyclomaticComplexity
      # rubocop:enable Metrics/PerceivedComplexity
    end
  end
end
