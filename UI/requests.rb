# frozen_string_literal: true

class Manager2cli
  class UI
    # Class to deal with user requests
    class Requests
      def initialize(api)
        @api = api
      end

      # List pending requests with all details
      def list
        CLI::UI::Frame.open('Pending requests') do
          resultset = getrequests

          puts 'No pending requests' if resultset['requests'] == []

          resultset['requests'].each_with_index do |request, index|
            CLI::UI::Frame.divider("Request id: #{index}")
            hashprint request
          end
        end
      end

      # rubocop:disable Metrics/CyclomaticComplexity
      # rubocop:disable Metrics/PerceivedComplexity
      # Approve a request
      def approve
        # rubocop:disable Metrics/BlockLength
        CLI::UI::Frame.open('Approve pending request') do
          # Get list of requests
          resultset = getrequests
          backout   = false

          # Show listing and get selection
          if resultset['requests'] == []
            puts 'No pending requests'
            backout = true
          end

          selected = nil
          unless backout
            selected = CLI::UI::Prompt.ask('Approve which request?') do |handler|
              resultset['requests'].each_with_index do |request, index|
                handler.option("#{request['user']} #{request['type']} (#{index})") { index }
              end
              handler.option('Go back') { backout = true }
            end
          end

          unless backout
            # Mapping of request types to API methods
            mappings = {
              'V1::Apache::Vhost::Request'  => '/v1/apache/vhost/new',
              'V1::Limit::Disk::Request'    => '/v1/limit/disk/set',
              'V1::Limit::Process::Request' => '/v1/limit/process/set',
              'V1::Limit::Cpu::Request'     => '/v1/limit/cpu/set',
              'V1::Limit::Memory::Request'  => '/v1/limit/memory/set',
              'V1::Mysql::Request'          => '/v1/mysql/new',
              'V1::Package::Request'        => '/v1/package/new'
            }

            # Build and execute API request
            details             = hashflatten(resultset['requests'][selected]['params'])
            details['username'] = resultset['requests'][selected]['user']

            result = Manager2cli::UI.apirequest(
              @api,
              mappings[resultset['requests'][selected]['type']],
              details,
              1
            )['result']

            # Include any extra information into email
            if result.is_a? Hash
              details = details.merge(hashflatten(result))
            else
              details['result'] = result
            end
            details.delete 'key'
            details.delete 'username' if details.key? 'username'
            details.delete 'dbname' if details.key? 'dbname'

            # Send approval email
            Manager2cli::UI.apirequest(
              @api,
              '/v1/notify/email/approved',
              {
                'username' => resultset['requests'][selected]['user'],
                'details'  => hashformat(details)
              },
              1
            )

            # Delete approved request
            deleterequest(selected)
          end
        end
        # rubocop:enable Metrics/BlockLength
      end
      # rubocop:enable Metrics/CyclomaticComplexity
      # rubocop:enable Metrics/PerceivedComplexity

      # Reject a request
      def reject
        # rubocop:disable Metrics/BlockLength
        CLI::UI::Frame.open('Reject pending request') do
          # Get list of requests
          resultset = getrequests
          backout   = false

          # Show listing and get selection
          if resultset['requests'] == []
            puts 'No pending requests'
            backout = true
          end

          selected = nil
          unless backout
            selected = CLI::UI::Prompt.ask('Reject which request?') do |handler|
              resultset['requests'].each_with_index do |request, index|
                handler.option("#{request['user']} #{request['type']} (#{index})") { index }
              end
              handler.option('Go back') { backout = true }
            end
          end

          unless backout
            # Add any comments
            comments = CLI::UI.ask('Provide any comments for rejection?', default: 'No additional comments provided')

            # Send rejection email
            Manager2cli::UI.apirequest(
              @api,
              '/v1/notify/email/rejected',
              {
                'username' => resultset['requests'][selected]['user'],
                'details'  => hashformat(resultset['requests'][selected]['params']),
                'comments' => comments
              },
              1
            )

            # Delete rejected request
            deleterequest(selected)
          end
        end
        # rubocop:enable Metrics/BlockLength
      end

      # Delete a request without further processing
      def delete
        CLI::UI::Frame.open('Delete request') do
          id = CLI::UI.ask('Delete request with which id?', allow_empty: false)

          deleterequest(id)
        end
      end

      private

      def getrequests
        Manager2cli::UI.apirequest(
          @api,
          '/v1/request/list',
          {},
          2
        )['result']
      end

      def deleterequest(id)
        Manager2cli::UI.apirequest(
          @api,
          '/v1/request/delete',
          { 'id' => id },
          1
        )
      end

      def hashprint(item)
        puts hashformat(item)
      end

      def hashflatten(item, output = {})
        item.each do |key, value|
          if value.instance_of?(Hash)
            hashflatten value, output
          else
            output[key] = value
          end
        end
        output
      end

      def hashformat(item, level = 0, output = '')
        item.each do |key, value|
          output += '  ' * level
          if value.instance_of?(Hash)
            output += "#{key}:\n"
            output += hashformat value, level + 1
          else
            output += "#{key}: #{value}\n"
          end
        end
        output
      end
    end
  end
end
